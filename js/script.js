$(document).ready(function () {

  $('.item').each(function (e, k) {

  });


  $('button.next').click(function () {
    var num = $('.item.active-1').data('id');
    if (num === $('.item').length - 1) {
      num = -1;
    }

    $('.item').removeClass('active').removeClass('active-1');

    $('.item').each(function (e, k) {
      for (let i = num + 1; i < num + 3; i++) {
        if (e === num + 1) {
          $(k).addClass('active-1');
        } else if (e <= num + 3 && e > num + 1) {
          $(k).addClass('active')
        } else if (num + 3 > $('.item').length - 1 && e === 0) {
          $(k).addClass('active')
        } else if (num + 3 > $('.item').length && e === 1) {
          $(k).addClass('active')
        }
      }
    });
  });

  $('button.previous').click(function () {
    var numX = $('.item.active-1').data('id');
    numX = numX - 1;
    if (numX === -1) {
      numX = $('.item').length - 1;
    }
    $('.item').removeClass('active').removeClass('active-1');
    $('.item').each(function (e, k) {

      if (e === numX) {
        $(k).addClass('active-1');
        if (numX === $('.item').length - 1) {
          $('.item').first().addClass('active');
          $('.item').first().next().addClass('active');
        }
        if (numX === $('.item').length - 2) {
          $('.item').first().addClass('active');
          $(k).next().addClass('active');
        } else {
          $(k).next().addClass('active');
          $(k).next().next().addClass('active');
        }
      }
    });
  });

});
